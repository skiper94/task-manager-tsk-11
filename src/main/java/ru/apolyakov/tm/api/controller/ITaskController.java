package ru.apolyakov.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();

    void showTaskByIndex();

    void showTaskById();

    void showTaskByName();

    void removeTaskByIndex();

    void removeTaskById();

    void removeTaskByName();

    void updateTaskByIndex();

    void updateTaskById();

}
