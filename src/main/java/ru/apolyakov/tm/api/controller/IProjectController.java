package ru.apolyakov.tm.api.controller;

import ru.apolyakov.tm.model.Project;

public interface IProjectController {

    void showProjects();

    void createProject();

    void clearProjects();

    void showProjectByIndex();

    void showProjectById();

    void showProject(Project project);

    void showProjectByName();

    void removeProjectByIndex();

    void removeProjectById();

    void removeProjectByName();

    void updateProjectByIndex();

    void updateProjectById();

}
