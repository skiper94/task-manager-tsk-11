package ru.apolyakov.tm.api.service;

import ru.apolyakov.tm.model.Task;
import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task add(String name, String description);

    void add(Task task);

    void remove(Task task);

    void clear();

    Task findOneById(String id);

    Task findOneByName(String name);

    Task findOneByIndex(Integer index);

    Task updateTaskById(String id, String name, String description);

    Task updateTaskByIndex(Integer index, String name, String description);

    Task removeOneByName(String name);

    Task removeOneById(String id);Task removeTaskByIndex(Integer index);

}
